#ifndef MATRIX_H
#define MATRIX_H

#include <stdio.h>
#include <stdlib.h>

#define copy_matrix_int(p, r, c) copy_matrix(p, r, c, sizeof(int), &print_int, vector_mult_int, data_sum_int)
#define copy_matrix_complex(p, r, c) copy_matrix(p, r, c, sizeof(double _Complex), &print_complex, vector_mult_complex, data_sum_complex)

struct Matrix {
    int rows;
    int cols;
    void*** data;
    
    void (*print_val)(void*);
    void *(*vector_mult)(void*, void*, int);
    void *(*sum)(void*, void *); 
};

typedef struct Matrix Matrix;

void print_complex(void*);

void print_int(void*);

void print(void*);

int check_sum(void*, void*);

int check_mult(void*, void*);

void* data_sum_int(void*, void*);

void* data_sum_complex(void*, void*);

void* vector_mult_int(void**, void**, int);

void* vector_mult_complex(void**, void**, int);

void* make_matrix(int, int);

void* copy_matrix(void*, int, int, int, void*, void*, void*);

void* transpose(void*);

void* matrix_sum(void*, void*);

void* matrix_mult(void*, void*);

void clear(void*);

#endif // MATRIX_H