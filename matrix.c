#include <stdio.h>
#include <stdlib.h>
#include <complex.h>

#include "matrix.h"

void* copy_matrix(void* nums, int rows, int cols, int size, void* print_val, void* vector_mult, void* sum) {

    Matrix* matrix = make_matrix(rows, cols);

    matrix->print_val = print_val;
    matrix->vector_mult = vector_mult;
    matrix->sum = sum; 

    for(int i=0; i<rows; i++) {
        for(int j=0; j<cols; j++) {
            matrix->data[i][j] = nums+(i*cols+j)*size ;  
        }
    }
    return matrix;  
}

void* make_matrix(int rows, int cols) {
    
    Matrix* matrix = malloc(sizeof(Matrix));

    void*** space = malloc(sizeof(void**) * rows);

    for (int i=0; i<rows; i++){
        space[i] = malloc(sizeof(void**));
        for (int j=0; j<cols; j++){
            space[i][j] = malloc(sizeof(void*));
        }
    }

    matrix->rows = rows;
    matrix->cols = cols;
    matrix->data = space;

    matrix->print_val = NULL;
    matrix->vector_mult = NULL;
    matrix->sum = NULL; 

    return matrix;
}

void print(void* a) {
    if (a!=NULL){
        Matrix* A = (Matrix *)a;
        for(int i = 0; i < A->rows; i++) {
            printf("\n");
            for(int j = 0; j < A->cols; j++) {
                A->print_val(A->data[i][j]);
            }
        }
        printf("\n");
    }
}

void print_int(void* A){
    printf("%d\t", *(int*)A); 
}

void print_complex(void* A){
    printf("%.1f %.1f*i\t", creal(*(double _Complex*)A), cimag(*(double _Complex*)A) ); 
}

int check_sum(void* a, void* b)
{
    Matrix* A = (Matrix *)a;
    Matrix* B = (Matrix *)b;

    if ((A->cols == B->cols) && (A->rows == B->rows)) {return 1;} else {return 0;};
};

int check_mult(void* a, void* b)
{
    Matrix* A = (Matrix *)a;
    Matrix* B = (Matrix *)b;

    if ((A->cols == B->rows) || (A->rows == B->cols)) {return 1;} else {return 0;};
};


void* vector_mult_int(void** a, void** b, int N)
{
    int* val = malloc(sizeof(int));

    for (int k=0; k < N; k++)
    {   
        *val += *(int*)(*(a+k)) * *(int*)(*(b+k));
    }

    return val;
}

void* vector_mult_complex(void** a, void** b, int N)
{
    double _Complex* val = malloc(sizeof(double _Complex));

    for (int k=0; k < N; k++)
    {  
        *val += *(double _Complex*)(*(a+k)) * *(double _Complex*)(*(b+k));
    }

    return val;
}

void* transpose(void* a)
{   
    Matrix* A = (Matrix *)a;
    int rows = A->rows;
    int cols = A->cols;

    Matrix* matrix = make_matrix(cols, rows);

    matrix->print_val = A->print_val;
    matrix->vector_mult = A->vector_mult;
    matrix->sum = A->sum;

    for (int i = 0; i < cols; i++){
        for (int j = 0; j< rows; j++){
            matrix->data[i][j] = A->data[j][i];
        }
    }
    return matrix;
}


void* matrix_sum(void* a, void* b)
{
    Matrix* A = (Matrix *)a;
    Matrix* B = (Matrix *)b;

    int rows = A->rows; 
    int cols = A->cols;

    if (check_sum(A, B))
    {   
        Matrix* matrix = make_matrix(rows, cols);

        matrix->print_val = A->print_val;
        matrix->vector_mult = A->vector_mult;
        matrix->sum = A->sum;

        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                matrix->data[i][j] = A->sum(A->data[i][j], B->data[i][j]);
            }
        }
        return matrix;
    }
    char output[] = "Матрицы нельзя сложить!!!";
    printf("%s\n", output);
    return NULL;
}


void* data_sum_int(void* a, void* b)
{
    void* p = malloc(sizeof(int));
    *(int*)p = *(int*)b + *(int*)a;
    return p;
}

void* data_sum_complex(void* a, void* b)
{
    void* p = malloc(sizeof(double _Complex));
    *(double _Complex*)p = *(double _Complex*)b + *(double _Complex*)a;
    return p;
}

void* matrix_mult(void* a, void* b)
{
    Matrix* A = (Matrix *)a;
    Matrix* B = (Matrix *)b;
    
    if (check_mult(A, B)){
        Matrix* matrix = make_matrix(A->rows, B->cols);

        matrix->print_val = A->print_val;
        matrix->vector_mult = A->vector_mult;
        matrix->sum = A->sum;
        
        Matrix* C = transpose(B);
        
        for (int i=0; i < A->rows; i++){
            for (int j = 0; j < B->cols; j++) {
                matrix->data[i][j] =  A->vector_mult(A->data[i], C->data[j], C->cols);
            }
        }
        return matrix;
    }
    char output[] = "Матрицы нельзя перемножить!!!";
    printf("%s\n", output);
    return NULL;
}

void clear(void *a){
    Matrix* A = (Matrix *)a;
    free(A->data);
    free(A);
}