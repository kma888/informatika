#include <stdio.h> 
#include <stdlib.h>
#include <complex.h>

#include "matrix.h"
#include "matrix.c"

int main(void) {

	int NUMS1[] = {
				10,20,30,
				40,50,60
				};
	
	int NUMS2[] = {
				-1,-2,-3,
				-4,-5,-6
				};

	int NUMS3[] = {
				99, 2,
				3, 4,
				5, 6
				};
	double _Complex Z1[] = {
        1.0 - 2.0 * I, -2.0 + 3.0 * I,
        3.0 - 5.0 * I, -3.0 + 6.0 * I,
        1.0 - 2.0 * I, -2.0 + 3.0 * I
    };

    double _Complex Z2[] = {
        0.0 + 2.0 * I, 7.0 - 2.0 * I,
        8.0 - 5.0 * I, 6.0 + 3.0 * I,
        7.7 - 1.0 * I, 9.0 + 2.9 * I
    };

    double _Complex Z3[] = {  
        7.0 + 2.0 * I, 7.0 - 2.0 * I, 4.0 + 0.0 * I,
        8.0 - 5.0 * I, 6.0 + 3.0 * I, 8.0 - 3.0 * I
    };

	Matrix* m1 = copy_matrix_int(NUMS1, 2, 3);
	Matrix* m2 = copy_matrix_int(NUMS2, 2, 3);
	Matrix* m3 = copy_matrix_int(NUMS3, 3, 2);
	Matrix* z1 = copy_matrix_complex(Z1, 2, 3);
	Matrix* z2 = copy_matrix_complex(Z2, 2, 3);
	Matrix* z3 = copy_matrix_complex(Z3, 3, 2);


	print(transpose(m1));
	print(transpose(z1));

	print(matrix_sum(m1, m2));
	print(matrix_sum(z1, z2));

	print(matrix_mult(m1, m3));
	print(matrix_mult(z1, z3));

	clear(m1);
	clear(m2);
	clear(m3);
	clear(z1);
	clear(z2);
	clear(z3);

    return 0;
}